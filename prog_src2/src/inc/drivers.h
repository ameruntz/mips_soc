#ifndef DRIVERS_H
#define DRIVERS_H 

#include <inttypes.h>
#define LED_ADDR 0xc0000000
#define SW_ADDR 0xd0000000


static inline void set_led(uint32_t val) {
  *((volatile uint32_t *) LED_ADDR) = val;
}

static inline uint32_t get_led() {
  return *((volatile uint32_t *) LED_ADDR);
}

static inline uint32_t get_sw() {
  return *((volatile uint32_t *) SW_ADDR);
}



#endif
