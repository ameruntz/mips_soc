`timescale 1ns / 1ps
  module RAM_wrapper(
      input  clock,
      input  reset,
      input         rea,
      input  [3:0]  wea,
      input  [17:0] addra,
      input  [31:0] dina,
      output [31:0] douta,
      output reg       dreadya,
      input         reb,
      input  [3:0]  web,
      input  [17:0] addrb,
      input  [31:0] dinb,
      output [31:0] doutb,
      output reg       dreadyb
      );

  parameter SIZE = 1024;
  parameter ADDR_WIDTH = 10;

     
  always @(posedge clock) begin
    dreadya <= (reset) ? 0 : ((wea != 4'b0000) ||  rea) ? 1 : 0;
    dreadyb <= (reset) ? 0 : ((web != 4'b0000) ||  reb) ? 1 : 0;
  end

    mem_we #(.SIZE(SIZE), .ADDR_WIDTH(ADDR_WIDTH)) RAM (
        .clk   (clock),   
        .weA    (wea),    
        .addrA  (addra),  
        .diA   (dina),    
        .doutA  (douta),  
        .weB    (web),    
        .addrB  (addrb),  
        .diB   (dinb),    
        .doutB  (doutb)   
    );

endmodule

