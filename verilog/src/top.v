module top(
    input  clk,
    input  reset_n,
    // I/O
    input  [7:0] Switch,
    output [7:0] Led
);
    
    
    // Clock signals
    wire clock, clock2x;
    wire clock_25MHz;
    wire PLL_Locked;
    
    reg reset;
    always @(posedge clock) begin
        reset <= reset_n | ~PLL_Locked;
    end
    
    // MIPS Processor Signals
    reg  [31:0] MIPS32_DataMem_In;
    wire [31:0] MIPS32_DataMem_Out, MIPS32_InstMem_In;
    wire [29:0] MIPS32_DataMem_Address, MIPS32_InstMem_Address;
    wire [3:0]  MIPS32_DataMem_WE;
    wire        MIPS32_DataMem_Read, MIPS32_InstMem_Read;
    reg         MIPS32_DataMem_Ready;
    wire [4:0]  MIPS32_Interrupts;
    wire        MIPS32_NMI;
    wire [7:0]  MIPS32_IP;
    wire        MIPS32_IO_WE;

    
    // BRAM Memory Signals
    reg  [3:0] BRAM_WEA;
    reg  BRAM_REA;
    reg  [17:0] BRAM_AddrA;
    reg  [31:0] BRAM_DINA;
    wire BRAM_ReadyA;
    wire BRAM_REB;
    wire [3:0] BRAM_WEB;
    wire [31:0] BRAM_DOUTB;
    wire BRAM_ReadyB;

   
    // LED Signals
    wire LED_WE;
    wire LED_RE;
    wire [7:0] LED_DOUT;
    wire LED_Ready;

    // Switch Input Signals
    wire Switches_RE;
    wire Switches_WE;
    wire Switches_Ready;
    wire [7:0] Switches_DOUT;

     // Clock Generation
    clk_gen Clock_Generator (
        .CLK_IN1  (clk),
        .RESET       (1'b0),
        .CLK_OUT1  (clock), 
        .CLK_OUT2  (clock2x),
        .CLK_OUT3  (clock_25MHz),
        .LOCKED   (PLL_Locked)
    );

    // MIPS-32 Core
    Processor MIPS32 (
        .clock            (clock),
        .reset            (reset),
        .Interrupts       (MIPS32_Interrupts),
        .NMI              (MIPS32_NMI),
        .DataMem_In       (MIPS32_DataMem_In),
        .DataMem_Ready    (MIPS32_DataMem_Ready),
        .DataMem_Read     (MIPS32_DataMem_Read),
        .DataMem_Write    (MIPS32_DataMem_WE),
        .DataMem_Address  (MIPS32_DataMem_Address),
        .DataMem_Out      (MIPS32_DataMem_Out),
        .InstMem_In       (MIPS32_InstMem_In),
        .InstMem_Address  (MIPS32_InstMem_Address),
        .InstMem_Ready    (BRAM_ReadyA),
        .InstMem_Read     (MIPS32_InstMem_Read),
        .IP               (MIPS32_IP)
    );

    // On-Chip Block RAM
    RAM_wrapper #(.SIZE(8192), .ADDR_WIDTH(13))
    Memory (
        .clock    (clock2x),
        .reset    (reset),
        .rea      (BRAM_REA),
        .wea      (BRAM_WEA),
        .addra    (BRAM_AddrA),
        .dina     (BRAM_DINA),
        .douta    (MIPS32_InstMem_In),
        .dreadya  (BRAM_ReadyA),
        .reb      (BRAM_REB),
        .web      (BRAM_WEB),
        .addrb    (MIPS32_DataMem_Address[17:0]),
        .dinb     (MIPS32_DataMem_Out),
        .doutb    (BRAM_DOUTB),
        .dreadyb  (BRAM_ReadyB)
    );

   
    // LED
    LED LEDs (
        .clock    (clock2x),
        .reset    (reset),
        .dataIn   (MIPS32_DataMem_Out[7:0]),
        .Write    (LED_WE),
        .Read     (LED_RE),
        .dataOut  (LED_DOUT),
        .Ack      (LED_Ready),
        .LedState (Led)
    );

    // Switch
    Switches Switches (
        .clock       (clock2x),
        .reset       (reset),
        .Read        (Switches_RE),
        .Write       (Switches_WE),
        .Switch_in   (Switch),
        .Ack         (Switches_Ready),
        .Switch_out  (Switches_DOUT)
    );
    
    
    assign MIPS32_IO_WE = (MIPS32_DataMem_WE == 4'hF) ? 1 : 0;
    assign MIPS32_Interrupts = 5'b00000;
    assign MIPS32_NMI = 0;

    always @(*) begin
        BRAM_REA   =  MIPS32_InstMem_Read;
        BRAM_WEA   =  4'h0;
        BRAM_AddrA =  MIPS32_InstMem_Address;
        BRAM_DINA  =  32'h0000_0000;
    end


    always @(*) begin
        case (MIPS32_DataMem_Address[29])
            0 : begin
                    MIPS32_DataMem_In    <= BRAM_DOUTB;
                    MIPS32_DataMem_Ready <= BRAM_ReadyB;
                end
            1 : begin
                    // Memory-mapped I/O
                    case (MIPS32_DataMem_Address[28:26])
                      // LED
                        3'b100 :    begin
                                        MIPS32_DataMem_In    <= {24'h000000, LED_DOUT[7:0]};
                                        MIPS32_DataMem_Ready <= LED_Ready;
                                    end
                        // Switches
                        3'b101 :    begin
                                        MIPS32_DataMem_In    <= {24'h000000, Switches_DOUT[7:0]};
                                        MIPS32_DataMem_Ready <= Switches_Ready;
                                    end
                        default:    begin
                                        MIPS32_DataMem_In    <= 32'h0000_0000;
                                        MIPS32_DataMem_Ready <= 0;
                                    end
                    endcase
                end
        endcase
    end
    
    // Memory
    assign BRAM_REB    = (MIPS32_DataMem_Address[29]) ? 0    : MIPS32_DataMem_Read;
    assign BRAM_WEB    = (MIPS32_DataMem_Address[29]) ? 4'h0 : MIPS32_DataMem_WE;
    // I/O
    assign LED_WE      = (MIPS32_DataMem_Address[29:26] == 4'b1100) ? MIPS32_IO_WE : 0;
    assign LED_RE      = (MIPS32_DataMem_Address[29:26] == 4'b1100) ? MIPS32_DataMem_Read : 0;
    assign Switches_WE = (MIPS32_DataMem_Address[29:26] == 4'b1101) ? MIPS32_IO_WE : 0;
    assign Switches_RE = (MIPS32_DataMem_Address[29:26] == 4'b1101) ? MIPS32_DataMem_Read : 0;
    
endmodule

