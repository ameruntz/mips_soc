module mem_we(clk, weA, addrA, diA, doutA, weB, addrB, diB, doutB);
parameter SIZE = 1024;
parameter ADDR_WIDTH = 10;
parameter COL_WIDTH = 8;
parameter NB_COL = 4;
input clk;
input [NB_COL-1:0] weA,weB;
input [ADDR_WIDTH-1:0] addrA,addrB;
input [NB_COL*COL_WIDTH-1:0] diA,diB;
output reg [NB_COL*COL_WIDTH-1:0] doutA,doutB;
reg [NB_COL*COL_WIDTH-1:0] RAM [SIZE-1:0];

initial
  $readmemh("memfile.dat",RAM);

always @(posedge clk)
  doutA <= RAM[addrA];

always @(posedge clk)
  doutB <= RAM[addrB];

generate
genvar i1;
for (i1 = 0; i1 < NB_COL; i1 = i1+1) 
  always @(posedge clk)
    if (weA[i1])
      RAM[addrA][(i1+1)*COL_WIDTH-1:i1*COL_WIDTH] <= diA[(i1+1)*COL_WIDTH-1:i1*COL_WIDTH];
endgenerate

generate
genvar i;
for (i = 0; i < NB_COL; i = i+1) 
  always @(posedge clk)
    if (weB[i])
      RAM[addrB][(i+1)*COL_WIDTH-1:i*COL_WIDTH] <= diB[(i+1)*COL_WIDTH-1:i*COL_WIDTH];
endgenerate

endmodule
