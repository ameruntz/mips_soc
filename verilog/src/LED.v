`timescale 1ns / 1ps

module LED(
    input  clock,
    input  reset,
    input  [7:0] dataIn,
    input  Write,
    input  Read,
    output [7:0] dataOut,
    output reg Ack,
    output [7:0] LedState
    );

    reg  [7:0] data;
    
    always @(posedge clock) begin
        data <= (reset) ? 8'b0 : ((Write) ? dataIn[7:0] : data);
    end
    
    always @(posedge clock) begin
        Ack <= (reset) ? 0 : (Write | Read);
    end
    
    assign LedState =  data;
    assign dataOut = data;

endmodule

