`timescale 1ns / 1ps

module Switches(
    input  clock,
    input  reset,
    input  Read,
    input  Write,
    input  [7:0] Switch_in, 
    output reg Ack,
    output [7:0] Switch_out
    );

    always @(posedge clock) begin
        Ack <= (reset) ? 0 : (Read | Write);
    end

    Switch_Filter Switch_Filter (
        .clock       (clock),
        .reset       (reset),
        .switch_in   (Switch_in),
        .switch_out  (Switch_out)
    );

endmodule

