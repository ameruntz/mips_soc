module testbench;
reg clk;
reg reset;
wire memwrite;
reg [7:0] Switch;
wire [7:0] Led;
top soc(clk,reset,Switch,Led);

initial begin
  $dumpfile("soc.vcd");
  $dumpvars(0,testbench);
end

initial begin
  $monitor("Led:%b Switch:%b time:%d",Led,Switch,$time);
  Switch = 0;
  reset = 0; 
  clk = 0;
  #600 Switch = 8'h04;
  #3600 Switch = 8'hf0;
  #6600 $finish;
end

always 
  #5 clk <= ~clk; 


endmodule
