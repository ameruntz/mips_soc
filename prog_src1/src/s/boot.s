	.abicalls
	.option	pic0
	.set	nomips16                


	.text
	.balign	4
	.global	boot
	.ent	boot
	.set	noreorder
boot:
	la	$t0, _bss_start		
	la	$t1, _bss_end
	la	$sp, _sp

BSS_CLEAR:
	beq	$t0, $t1, CP0_SETUP	
	nop
	sb	$0, 0($t0)
	j	BSS_CLEAR
	addiu	$t0, $t0, 1

CP0_SETUP:
	la	$26, fun
	mtc0	$26, $30, 0		# ErrorEPC gets address of fun
	mfc0	$26, $13, 0		# Load Cause register
	lui	$27, 0x0080		# Use "special" interrupt vector
	or	$26, $26, $27
	mtc0	$26, $13, 0		# Commit new Cause register
	mfc0	$26, $12, 0		# Load Status register
	lui	$27, 0x0fff		# Disable access to Coprocessors
	ori	$27, $27, 0x00ee	# Disable all interrupts,
	and	$26, $26, $27		#   and set kernel mode
	mtc0	$26, $12, 0		# Commit new Status register
	eret				# Return from Reset Exception

fun:
  jal main
  nop

L1: 
 	j L1 
  nop

.end boot

