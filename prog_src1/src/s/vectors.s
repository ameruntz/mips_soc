	.abicalls
	.option	pic0
	.set	nomips16                


	.text
	.balign	4
	.ent	exception_vector
	.set	noreorder
exception_vector:
	j exception_vector	
	nop
	.end	exception_vector


	.ent	interrupt_vector
interrupt_vector:
	j interrupt_vector	
	nop
	.end	interrupt_vector

